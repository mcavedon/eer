"""
Routine to evaluate Er automatically from CMZ and CPZ
"""
import sys, os
sys.dont_write_bytecode=True
import matplotlib.pylab as plt
import numpy as np
import sys
import EER.lib.ConstrainedSpline as cs
from EER.lib import CXZ, IDA, interp, OnClick, Ipsh
import logging
import getpass
#import tensorflow as tf
#import gpflow

# ------------ If error occurs then drop into debugging shell
# ------------ (to disable comment out the following lines)
from IPython.core import ultratb
sys.excepthook = ultratb.FormattedTB(mode='Verbose',
                      color_scheme='Linux', call_pdb=1)
# -----------------------------------------------------------

logger = logging.getLogger()

class EER:
    def __init__(self,CMZ,CPZ,tBeg=-10,tEnd=10):
        self.CMZ = CMZ
        self.CPZ = CPZ
        # Check if shot is the same
        if (self.CMZ.shot != self.CPZ.shot):
            raise ValueError('CMZ and CPZ from different shot numbers')
        # Check if measuring same impurity
        if (self.CMZ.atom != self.CPZ.atom) or \
           (self.CMZ.charge != self.CPZ.charge):
               pass
            #raise ValueError('CMZ and CPZ measuring different impurities')
        self.shiftCPZ = self.CPZ.shift
        self.shiftCMZ = self.CMZ.shift

    def check_alignment(self,tBeg=-10,tEnd=10,x='R'):
        tmpCMZ = self.CMZ.selectTime(tBeg,tEnd)
        tmpCPZ = self.CPZ.selectTime(tBeg,tEnd)
        f = plt.figure()
        ax = f.add_subplot(111)
        tmpCMZ.plotProfile('Ti_c',x=x,ax=ax,color='r',ls='',fmt='o',label='CMZ')
        tmpCPZ.plotProfile('Ti_c',x=x,ax=ax,color='b',ls='',fmt='o',label='CPZ')
        ax.legend()
        plt.show()

    def fit_profiles(self,plot=False,plot_IDA=False,sTi=2.7,sInte=30,svtor=2,\
            rhoRan=[0.,1.01],TiDiags=['CPZ','CMZ'],InteDiags=['CPZ','CMZ']):
        # if logger level in DEBUG set interactive mode
        self.sTi = sTi
        self.sInte = sInte
        self.svtor = svtor
        self.Ti = [] # list of splines for Ti
        self.inte = [] # list of splines for nimp
        self.vtor = []
        self.Ti_GP = [] # list of GP for Ti
        self.inte_GP = [] # list of GP for inte
        self.vtor_GP = []
        self.rhoRan = rhoRan
        self.ti_sigma = np.zeros(self.CPZ.nt) * np.nan # Standard desviation for Ti
        self.inte_sigma = np.zeros(self.CPZ.nt) * np.nan # Standard desviation for nimp
        nPerc = 5
        logging.warning('Fitting...')
        dtCPZ = np.diff(self.CPZ.time).mean()
        for jtCPZ in range(self.CPZ.nt):
            logging.warning('%d/%d'%(jtCPZ,self.CPZ.nt))
            if self.CPZ.nt > nPerc:
                if jtCPZ%(np.int(self.CPZ.nt/nPerc)) == 0 and jtCPZ != 0:
                    logging.warning(r'%d/100'%(np.float(jtCPZ+1)/self.CPZ.nt*100))
            # ----------- Look for the closest CMZ time point if anything below 1.e-3 s
            jtCMZ = np.argmin(np.abs(self.CPZ.time[jtCPZ]-self.CMZ.time))
            if (np.abs(self.CPZ.time[jtCPZ]-self.CMZ.time[jtCMZ])>dtCPZ):
                logging.warning(r'Time bases are different')
                logging.warning(r'Discarding time point %.3f s'%self.CPZ.time[jtCPZ])
                self.Ti.append(np.nan)
                self.Ti_GP.append(np.nan)
                self.inte.append(np.nan)
                self.inte_GP.append(np.nan)
                self.vtor.append(np.nan)
                self.vtor_GP.append(np.nan)
                continue

            # ----------- Find rho within the accepted range
            rhoIndexCPZ = np.arange(self.CPZ.nch)\
                    [(self.CPZ.Ti_c.area.dat[jtCPZ] >= rhoRan[0])*(self.CPZ.Ti_c.area.dat[jtCPZ] <= rhoRan[1])]
            rhoIndexCMZ = np.arange(self.CMZ.nch)\
                    [(self.CMZ.Ti_c.area.dat[jtCMZ] >= rhoRan[0])*(self.CMZ.Ti_c.area.dat[jtCMZ] <= rhoRan[1])]

            # ---------- Create and sort areabase as a sum of CMZ and CPZ measuraments
            x = np.array([])
            y = np.array([])
            dy = np.array([])
            if 'CPZ' in TiDiags:
                x = np.append(x,self.CPZ.Ti_c.area.rMaj[jtCPZ,rhoIndexCPZ])
                y = np.append(y,self.CPZ.Ti_c.dat[jtCPZ,rhoIndexCPZ])
                dy = np.append(dy,self.CPZ.err_Ti_c.dat[jtCPZ,rhoIndexCPZ])
            if 'CMZ' in TiDiags:
                x = np.append(x,self.CMZ.Ti_c.area.rMaj[jtCMZ,rhoIndexCMZ])
                y = np.append(y,self.CMZ.Ti_c.dat[jtCMZ,rhoIndexCMZ])
                dy = np.append(dy,self.CMZ.err_Ti_c.dat[jtCMZ,rhoIndexCMZ])
            indexSort = np.argsort(x)
            x = x[indexSort]
            xbase = x.copy()
            # ---------- Fit of the ion temperature
            y = y[indexSort]
            dy = dy[indexSort]
            # Sort out nans
            if np.where(np.isnan(y))[0].size > 0:
                nan_indexes = np.where(np.isnan(y))
                y = np.delete(y,nan_indexes)
                dy = np.delete(dy,nan_indexes)
                x = np.delete(xbase,nan_indexes)
            # Smoothing constant optimization
            rho_CPZ = np.array(self.CPZ.Ti_c.area.dat[jtCPZ,rhoIndexCPZ])
            data_CPZ = np.array(self.CPZ.Ti_c.dat[jtCPZ,rhoIndexCPZ])
            rho_CMZ = np.array(self.CMZ.Ti_c.area.dat[jtCMZ,rhoIndexCMZ])
            data_CMZ = np.array(self.CMZ.Ti_c.dat[jtCMZ,rhoIndexCMZ])
            if np.where(np.isnan(data_CPZ))[0].size > 0:
                nan_indexes = np.where(np.isnan(data_CPZ))
                rho_CPZ = np.delete(rho_CPZ,nan_indexes)
                data_CPZ = np.delete(data_CPZ,nan_indexes)
            if np.where(np.isnan(data_CMZ))[0].size > 0:
                nan_indexes = np.where(np.isnan(data_CMZ))
                rho_CMZ = np.delete(rho_CMZ,nan_indexes)
                data_CMZ = np.delete(data_CMZ,nan_indexes)
            ti_sigma = 0.0
            if rho_CPZ.size == 0 or rho_CMZ.size == 0:
                self.Ti.append(np.nan)
                self.Ti_GP.append(np.nan)
                self.inte.append(np.nan)
                self.inte_GP.append(np.nan)
                self.vtor.append(np.nan)
                self.vtor_GP.append(np.nan)
                logging.warning("Data are nans")
                continue
            for jCPZ in range(np.size(rho_CPZ)):
                jCMZ = np.where( rho_CMZ == rho_CMZ[np.argmin(np.abs(rho_CMZ-rho_CPZ[jCPZ]))])[0]
                jCMZ = jCMZ[0]
                ti_sigma += np.abs(data_CPZ[jCPZ]-data_CMZ[jCMZ])
            ti_sigma = ti_sigma/np.size(rho_CPZ)
            self.ti_sigma[jtCPZ] = ti_sigma

            try:
                spline = cs.spline_negative_derivative(x,y,k=3,w=1./dy,s=dy.size*sTi*ti_sigma)
                #Ti_GP = fit_gpflow.FitGpFlow(x,y,stdfit=spline)
                self.Ti.append(spline)
                #self.Ti_GP.append(Ti_GP)
                self.Ti_GP.append(spline)
            except:
                self.Ti.append(np.nan)
                self.Ti_GP.append(np.nan)
                self.vtor.append(np.nan)
                self.vtor_GP.append(np.nan)
                self.inte.append(np.nan)
                self.inte_GP.append(np.nan)
                logging.warning("Ti spline fit failed")
                continue

            # ---------- Fit of the intensity
            x = np.array([])
            y = np.array([])
            dy = np.array([])
            if 'CPZ' in TiDiags:
                x = np.append(x,self.CPZ.inte.area.rMaj[jtCPZ,rhoIndexCPZ])
                y = np.append(y,self.CPZ.inte.dat[jtCPZ,rhoIndexCPZ])
                dy = np.append(dy,self.CPZ.err_inte.dat[jtCPZ,rhoIndexCPZ])
            if 'CMZ' in TiDiags:
                x = np.append(x,self.CMZ.inte.area.rMaj[jtCMZ,rhoIndexCMZ])
                y = np.append(y,self.CMZ.inte.dat[jtCMZ,rhoIndexCMZ])
                dy = np.append(dy,self.CMZ.err_inte.dat[jtCMZ,rhoIndexCMZ])
            x = x[indexSort]
            y = y[indexSort]
            dy = dy[indexSort]
            xbase = x.copy()
            # Sort out nans
            if np.where(np.isnan(y))[0].size > 0:
                nan_indexes = np.where(np.isnan(y))
                y = np.delete(y,nan_indexes)
                dy = np.delete(dy,nan_indexes)
                x = np.delete(xbase,nan_indexes)
            # Smoothing constant optimization
            rho_CPZ = np.array(self.CPZ.inte.area.dat[jtCPZ,rhoIndexCPZ])
            data_CPZ = np.array(self.CPZ.inte.dat[jtCPZ,rhoIndexCPZ])
            rho_CMZ = np.array(self.CMZ.inte.area.dat[jtCMZ,rhoIndexCMZ])
            data_CMZ = np.array(self.CMZ.inte.dat[jtCMZ,rhoIndexCMZ])
            if np.where(np.isnan(data_CPZ))[0].size > 0:
                nan_indexes = np.where(np.isnan(data_CPZ))
                rho_CPZ = np.delete(rho_CPZ,nan_indexes)
                data_CPZ = np.delete(data_CPZ,nan_indexes)
            if np.where(np.isnan(data_CMZ))[0].size > 0:
                nan_indexes = np.where(np.isnan(data_CMZ))
                rho_CMZ = np.delete(rho_CMZ,nan_indexes)
                data_CMZ = np.delete(data_CMZ,nan_indexes)
            inte_sigma = 0.0
            for jCPZ in range(np.size(rho_CPZ)):
                jCMZ = np.where( rho_CMZ == rho_CMZ[np.argmin(np.abs(rho_CMZ-rho_CPZ[jCPZ]))])[0]
                jCMZ = jCMZ[0]
                inte_sigma = inte_sigma + np.abs(data_CPZ[jCPZ]-data_CMZ[jCMZ])
            inte_sigma = inte_sigma/np.size(rho_CPZ)
            self.inte_sigma[jtCPZ]=inte_sigma
            try:
                spline = cs.spline_negative_derivative(x,y,k=3,w=1./dy,s=dy.size*sInte*inte_sigma)
                #inte_GP = fit_gpflow.FitGpFlow(x,y,stdfit=spline)
                #self.inte_GP.append(inte_GP)
                self.inte_GP.append(spline)
                self.inte.append(spline)
            except:
                self.Ti[jtCPZ] = np.nan
                self.Ti_GP[jtCPZ] = np.nan
                self.vtor.append(np.nan)
                self.vtor_GP.append(np.nan)
                self.inte.append(np.nan)
                self.inte_GP.append(np.nan)
                logging.warning("Inte spline fit failed")
                continue

            # Fit vtor
            x = self.CMZ.vr_c.area.rMaj[jtCMZ,rhoIndexCMZ]
            y = self.CMZ.vr_c.dat[jtCMZ,rhoIndexCMZ]
            dy = self.CMZ.err_vr_c.dat[jtCMZ,rhoIndexCMZ]
            indexSort = np.argsort(x)
            x = x[indexSort]
            xbase = x.copy()
            y = y[indexSort]
            dy = dy[indexSort]
            # Sort out nans
            if np.where(np.isnan(y))[0].size > 0:
                nan_indexes = np.where(np.isnan(y))
                y = np.delete(y,nan_indexes)
                dy = np.delete(dy,nan_indexes)
                x = np.delete(xbase,nan_indexes)
            try:
                spline = cs.spline_null_center(x,y,k=3,w=1./dy,s=dy.size*svtor)
                #vtor_GP = fit_gpflow.FitGpFlow(np.double(x),np.double(y),stdfit=spline,lengthscale=1)
                #self.vtor_GP.append(vtor_GP)
                self.vtor_GP.append(spline)
                self.vtor.append(spline)
            except:
                self.Ti[jtCPZ] = np.nan
                self.Ti_GP[jtCPZ] = np.nan
                self.vtor.append(np.nan)
                self.vtor_GP.append(np.nan)
                self.inte[jtCPZ] = np.nan
                self.inte_GP[jtCPZ] = np.nan
                logging.warning("vtor spline fit failed")
                continue


            # plot if plot=True or logger level to INFO or DEBUG
            if plot:
                f = plt.figure(figsize=(12,4))
                ax1 = f.add_subplot(131)
                ax2 = f.add_subplot(132)
                ax3 = f.add_subplot(133)
                ax1.set_title('CPZ time %.5f'%(self.CPZ.time[jtCPZ]))
                ax2.set_title('CMZ time %.5f'%(self.CMZ.time[jtCMZ]))
                if 'CPZ' in TiDiags:
                    ax1.errorbar(self.CPZ.Ti_c.area.dat[jtCPZ,rhoIndexCPZ],self.CPZ.Ti_c.dat[jtCPZ,rhoIndexCPZ],\
                                self.CPZ.err_Ti_c.dat[jtCPZ,rhoIndexCPZ],fmt='o',label='CPZ',color='r')
                if 'CMZ' in TiDiags:
                    ax1.errorbar(self.CMZ.Ti_c.area.dat[jtCMZ,rhoIndexCMZ],self.CMZ.Ti_c.dat[jtCMZ,rhoIndexCMZ],\
                                self.CMZ.err_Ti_c.dat[jtCMZ,rhoIndexCMZ],fmt='o',label='CMZ',color='b')
                if 'CPZ' in InteDiags:
                    ax2.errorbar(self.CPZ.inte.area.dat[jtCPZ,rhoIndexCPZ],self.CPZ.inte.dat[jtCPZ,rhoIndexCPZ],\
                                self.CPZ.err_inte.dat[jtCPZ,rhoIndexCPZ],fmt='o',label='CPZ',color='r')
                if 'CMZ' in InteDiags:
                    ax2.errorbar(self.CMZ.inte.area.dat[jtCMZ,rhoIndexCMZ],self.CMZ.inte.dat[jtCMZ,rhoIndexCMZ],\
                                self.CMZ.err_inte.dat[jtCMZ,rhoIndexCMZ],fmt='o',label='CMZ',color='b')
                ax3.errorbar(self.CMZ.vr_c.area.dat[jtCMZ,rhoIndexCMZ],self.CMZ.vr_c.dat[jtCMZ,rhoIndexCMZ],\
                            self.CMZ.err_vr_c.dat[jtCMZ,rhoIndexCMZ],fmt='o',label='CMZ',color='b')
                xlims = [np.nanmin(np.append(self.CPZ.Ti_c.area.dat[jtCPZ,rhoIndexCPZ],self.CMZ.Ti_c.area.dat[jtCMZ,rhoIndexCMZ])),\
                         np.nanmax(np.append(self.CPZ.Ti_c.area.dat[jtCPZ,rhoIndexCPZ],self.CMZ.Ti_c.area.dat[jtCMZ,rhoIndexCMZ]))]
                ylims1 = [0,np.nanmax(np.append(self.CPZ.Ti_c.dat[jtCPZ,rhoIndexCPZ],self.CMZ.Ti_c.dat[jtCMZ,rhoIndexCMZ]))]
                ylims2 = [0,np.nanmax(np.append(self.CPZ.inte.dat[jtCPZ,rhoIndexCPZ],self.CMZ.inte.dat[jtCMZ,rhoIndexCMZ]))]
                ylims3 = [np.nanmin(self.CMZ.vr_c.dat[jtCMZ,rhoIndexCMZ]),np.nanmax(self.CMZ.vr_c.dat[jtCMZ,rhoIndexCMZ])]
                ax1.set_xlim(xlims)
                ax1.set_ylim(ylims1)
                ax2.set_xlim(xlims)
                ax2.set_ylim(ylims2)
                ax3.set_xlim(xlims)
                ax3.set_ylim(ylims3)
                gridIndex = np.arange(self.CPZ.ngrid)\
                    [(self.CPZ.gridRho[jtCPZ] >= rhoRan[0])\
                    *(self.CPZ.gridRho[jtCPZ] <= rhoRan[1])\
                    *(self.Ti[jtCPZ](self.CPZ.gridR[jtCPZ])>0.0)]
                ax1.plot(self.CPZ.gridRho[jtCPZ,gridIndex],self.Ti[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex]),lw=3,label='spline_Ti',color='g')
                try:
                    y,dy = self.Ti_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex])
                except:
                    y = self.Ti_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex])
                    dy = 0.1*y
                ax1.errorbar(self.CPZ.gridRho[jtCPZ,gridIndex],y,dy,label='GP')
                if plot_IDA:
                    jtIDA = np.where(self.IDA.time == self.IDA.time[np.argmin(np.abs(self.IDA.time\
                             -self.CPZ.time[jtCPZ]))])[0]
                    jtIDA=jtIDA[0]
                    gridIndexIDA = np.arange(np.size(self.IDA.ne.area.dat[jtIDA]))\
                        [(self.IDA.Te.area.dat[jtIDA] >= 0.93)\
                        *(self.IDA.Te.area.dat[jtIDA] <= 1.01)\
                        *(self.IDA.Te.dat[jtIDA] > 0)]

                    ax1.plot(self.IDA.Te.area.dat[jtIDA,gridIndexIDA],self.IDA.Te.dat[jtIDA,gridIndexIDA],lw=3,label='IDA_Te')
                ax2.plot(self.CPZ.gridRho[jtCPZ,gridIndex],self.inte[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex]),lw=3,label='spline',color='g')
                try:
                    y,dy = self.inte_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex])
                except:
                    y = self.inte_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex])
                    dy = y*0.1
                ax2.errorbar(self.CPZ.gridRho[jtCPZ,gridIndex],y,dy,label='GP')
                ax3.plot(self.CPZ.gridRho[jtCPZ,gridIndex],self.vtor[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex]),lw=3,label='spline',color='g')
                try:
                    y,dy = self.vtor_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex])
                except:
                    y = self.vtor_GP[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex],ext=3)
                    dy = y*0.1
                ax3.errorbar(self.CPZ.gridRho[jtCPZ,gridIndex],y,dy,label='GP')
                ax1.legend()
                ax2.legend()
                ax3.legend()
                plt.show()

    def calculate_er(self,plot=False):
        # Calculate Er and all the othe quantities of the radial force balance
        # on the CPZ grid
        self.sf_Er = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_Er = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_diaImp = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_diaImp = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_dTidr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_Ti = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_dTidr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_Ti = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_dintdr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_int = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_int = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_dintdr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_vpol = self.CPZ.vr_c.dat
        self.sf_s_vpol = self.CPZ.err_vr_c.dat
        self.sf_vtor = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_s_vtor = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_Te = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_dTe_dr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_ne = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_dne_dr = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_diaEl = np.zeros((self.CPZ.nt,self.CPZ.nch)) * np.nan
        self.sf_Bp = self.CPZ.Bp[:,:]
        self.sf_Bt = self.CPZ.Bt[:,:]
        self.sf_rhoPol = self.CPZ.Ti_c.area.dat
        self.sf_rMaj = self.CPZ.Ti_c.area.rMaj
        self.sf_time = self.CPZ.time
        self.prueba=0
        for jt in range(self.CPZ.nt):
            print(jt)
            if type(self.Ti[jt]) == np.float or type(self.inte[jt]) == np.float:
                continue
            try:
                self.sf_Ti[jt],self.sf_s_Ti[jt] = self.Ti_GP[jt](self.sf_rMaj[jt])
                self.sf_dTidr[jt],self.sf_s_dTidr[jt] = self.Ti_GP[jt](self.sf_rMaj[jt],nu=1)
                self.sf_int[jt],self.sf_s_int[jt] = self.inte_GP[jt](self.sf_rMaj[jt])
                self.sf_dintdr[jt],self.sf_s_dintdr[jt] = self.inte_GP[jt](self.sf_rMaj[jt],nu=1)
                self.sf_vtor[jt],self.sf_s_vtor[jt] = self.vtor_GP[jt](self.sf_rMaj[jt])
            except:
                self.sf_Ti[jt] = self.Ti_GP[jt](self.sf_rMaj[jt],ext=3)
                self.sf_s_Ti[jt] = self.sf_Ti[jt]*0.1
                self.sf_dTidr[jt] = self.Ti_GP[jt](self.sf_rMaj[jt],nu=1,ext=3)
                self.sf_s_dTidr[jt] = self.sf_dTidr[jt]*0.1
                self.sf_int[jt] = self.inte_GP[jt](self.sf_rMaj[jt],ext=3)
                self.sf_s_int[jt] = self.sf_int[jt]*0.1
                self.sf_dintdr[jt] = self.inte_GP[jt](self.sf_rMaj[jt],nu=1,ext=3)
                self.sf_s_dintdr[jt] = self.sf_dintdr[jt]*0.1
                self.sf_vtor[jt] = self.vtor_GP[jt](self.sf_rMaj[jt],ext=3)
                self.sf_s_vtor[jt] = self.sf_vtor[jt]*0.1
            if hasattr(self,"IDA"):
                self.sf_Te[jt] = interp.extrap(self.sf_rhoPol[jt],self.IDA.Te.area.dat[jtIDA],self.IDA.Te.dat[jtIDA])
                self.sf_dTe_dr[jt] = interp.extrap(self.sf_rhoPol[jt],self.IDA.dTe_dr.area.dat[jtIDA],self.IDA.dTe_dr.dat[jtIDA])
                self.sf_ne[jt] = interp.extrap(self.sf_rhoPol[jt],self.IDA.ne.area.dat[jtIDA],self.IDA.ne.dat[jtIDA])
                self.sf_dne_dr[jt] = interp.extrap(self.sf_rhoPol[jt],self.IDA.dne_dr.area.dat[jtIDA],self.IDA.dne_dr.dat[jtIDA])
                self.sf_diaEl[jt] = (self.sf_dTe_dr[jt]+self.sf_Te[jt]*self.sf_dne_dr[jt]/self.sf_ne[jt])/self.CPZ.charge
            self.sf_diaImp[jt] = (self.sf_dTidr[jt]+\
                               self.sf_Ti[jt]*self.sf_dintdr[jt]/self.sf_int[jt])/self.CPZ.charge
            self.sf_s_diaImp[jt] = 1/self.CPZ.charge*np.sqrt(\
                    self.sf_s_dTidr[jt]**2+\
                    (self.sf_s_Ti[jt]*self.sf_dintdr[jt]/self.sf_int[jt])**2+\
                    (self.sf_Ti[jt]*self.sf_s_dintdr[jt]/self.sf_int[jt])**2+\
                    (self.sf_Ti[jt]*self.sf_dintdr[jt]*self.sf_s_int[jt]/self.sf_int[jt]**2)**2)
            self.sf_Er[jt] = self.sf_diaImp[jt] + self.sf_Bp[jt]*self.sf_vtor[jt] - \
                    self.sf_Bt[jt]*self.sf_vpol[jt]
            self.sf_s_Er[jt] = np.sqrt(\
                    self.sf_s_diaImp[jt]**2 + \
                    (self.sf_Bp[jt]*self.sf_s_vtor[jt])**2 + \
                    (self.sf_Bt[jt]*self.sf_s_vpol[jt])**2)
            rhoIndexNan = np.arange(self.CPZ.nch)\
                    [(self.sf_rhoPol[jt] <= self.rhoRan[0])]
            rhoIndexNan = np.append(rhoIndexNan,np.arange(self.CPZ.nch)\
                                    [(self.sf_rhoPol[jt] >= self.rhoRan[1])])
            rhoIndexNegTi = np.arange(self.CPZ.nch)[self.sf_Ti[jt]<0]
            for profile in ['Ti','s_Ti','dTidr','s_dTidr',\
                    'int','s_int','dintdr','s_dintdr',\
                    'vtor','s_vtor','vpol','s_vpol','diaImp','s_diaImp','Er','s_Er']:
                self.__dict__['sf_'+profile][jt,rhoIndexNan] = np.nan
            #Put a Nan where Ti<0 to not calculate Er
            self.__dict__['sf_Ti'][jt,rhoIndexNegTi] = np.nan
            self.__dict__['sf_dTidr'][jt,rhoIndexNegTi] = np.nan
        if plot:
            for jt in range(self.CPZ.nt):
                f = plt.figure()
                OnClick.Click2Zoom(f)
                ax = f.add_subplot(611)
                ax.set_title('Time: %.5fs'%self.sf_time[jt])
                ax.errorbar(self.sf_rhoPol[jt],self.sf_Er[jt],self.sf_s_Er[jt],fmt='o',label='$E_r$')
                ax.plot(self.sf_rhoPol[jt],self.sf_diaImp[jt],'x-',label='DiaImp')
                ax.plot(self.sf_rhoPol[jt],-self.sf_vpol[jt]*self.sf_Bt[jt],'D-',\
                        label='$-v_{pol}*B_{tor}$')
                ax.plot(self.sf_rhoPol[jt],self.sf_vtor[jt]*self.sf_Bp[jt],'>-',\
                        label='$v_{tor}*B_{pol}$')
                ax.plot(self.sf_rhoPol[jt],self.sf_diaEl[jt],'x-',label='DiaEl')
                ax.legend()
                ax = f.add_subplot(612,sharex=ax)
                ax.errorbar(self.sf_rhoPol[jt],self.sf_Ti[jt],self.sf_s_Ti[jt],fmt='o-',label='Ti')
                ax.legend()
                ax = f.add_subplot(613,sharex=ax)
                ax.errorbar(self.sf_rhoPol[jt],self.sf_dTidr[jt],self.sf_s_dTidr[jt],fmt='o-',label='dTidr')
                ax.legend()
                ax = f.add_subplot(614,sharex=ax)
                ax.errorbar(self.sf_rhoPol[jt],self.sf_diaImp[jt],self.sf_s_diaImp[jt],fmt='o',\
                        label='DiaImp')
                ax.legend()
                ax = f.add_subplot(615,sharex=ax)
                ax.errorbar(self.sf_rhoPol[jt],self.sf_vpol[jt],self.sf_s_vpol[jt],fmt='o-',label='vpol')
                ax.legend()
                ax = f.add_subplot(616,sharex=ax)
                ax.errorbar(self.sf_rhoPol[jt],self.sf_vtor[jt],self.sf_s_vtor[jt],fmt='o-',label='vtor')
                ax.legend()
                plt.show()


    def write_sf(self,experiment=getpass.getuser().upper(),path2sh=None):
        """
        Write results into a shotfile EER
        """
        from EER.lib import sfh
        import shutil
        if path2sh is None:
            dir_path = os.path.dirname(path)
        else:
            dir_path = path2sh
        sfh = sfh.SFH()
        diagnostic = 'EER'
        # Change number of channels in sfh accordingly to CPZ channels
        sfh.Open(dir_path+'/'+diagnostic+'00000.sfh')
        sfh.Mdindex('Er',self.CPZ.nch,0,0)
        sfh.Mdindex('s_Er',self.CPZ.nch,0,0)
        sfh.Mdindex('diaImp',self.CPZ.nch,0,0)
        sfh.Mdindex('s_diaImp',self.CPZ.nch,0,0)
        sfh.Mdindex('dTidr',self.CPZ.nch,0,0)
        sfh.Mdindex('s_dTidr',self.CPZ.nch,0,0)
        sfh.Mdindex('Ti',self.CPZ.nch,0,0)
        sfh.Mdindex('s_Ti',self.CPZ.nch,0,0)
        sfh.Mdindex('dintdr',self.CPZ.nch,0,0)
        sfh.Mdindex('s_dintdr',self.CPZ.nch,0,0)
        sfh.Mdindex('int',self.CPZ.nch,0,0)
        sfh.Mdindex('s_int',self.CPZ.nch,0,0)
        sfh.Mdindex('vpol',self.CPZ.nch,0,0)
        sfh.Mdindex('s_vpol',self.CPZ.nch,0,0)
        sfh.Mdindex('vtor',self.CPZ.nch,0,0)
        sfh.Mdindex('s_vtor',self.CPZ.nch,0,0)
        sfh.Mdindex('Bp',self.CPZ.nch,0,0)
        sfh.Mdindex('Bt',self.CPZ.nch,0,0)
        sfh.Mdindex('rhoPol',self.CPZ.nch,0,0)
        sfh.Mdindex('rMaj',self.CPZ.nch,0,0)
        sfh.Close()
        try:
            shutil.copy(dir_path+'/'+diagnostic+'00000.sfh','.')
        except:
            pass

        from EER.lib import ww
        shot = self.CMZ.shot
        sf = ww.shotfile()
        sf.Open(experiment,diagnostic,shot)
        sf.SetSignal('time',np.array(self.sf_time,dtype=np.float))
        sf.SetParameter('Info','UserID',os.environ['USER'])
        sf.SetParameter('Info','wvl_cx',self.CMZ.wvl_cx)
        sf.SetParameter('Info','imp_cx',self.CMZ.atom)
        sf.SetParameter('Info','imp_Z',np.array(self.CMZ.charge,dtype=np.int))
        sf.SetParameter('Info','shiftCPZ',np.array(self.shiftCPZ,dtype=np.float))
        sf.SetParameter('Info','shiftCMZ',np.array(self.shiftCMZ,dtype=np.float))
        sf.SetParameter('Info','sTi',np.array(self.sTi,dtype=np.float))
        sf.SetParameter('Info','sInte',np.array(self.sInte,dtype=np.float))
        sf.SetParameter('Info','sVtor',np.array(self.sInte,dtype=np.float))
        sf.SetParameter('Info','DIAG_EQ',self.CPZ.DiagEq)
        sf.SetParameter('Info','EXP_EQ',self.CPZ.ExpEq)
        sf.SetParameter('Info','ED_EQ',self.CPZ.EdEq)
        sf.SetParameter('Info','rhoRan',np.array(self.rhoRan,dtype=np.float))
        sf.SetParameter('Info','tSync',np.array(self.CPZ.tmid,dtype=np.float))
        for signal in self.__dict__.keys():
            if signal[:3] == "sf_" and signal[3:] != 'time':
                if 'Te' in signal or 'ne' in signal or 'diaEl' in signal:
                    pass
                else:
                    sf.SetSignalGroup(signal[3:].replace("sigma","s"),np.array(self.__dict__[signal],dtype=np.float))
        sf.Close()
        os.remove(diagnostic+'00000.sfh')

    def fixed_grid(self,plot=False,time_trace=False,rho_plot=None):
        npoints = 1000
        self.fix_grid = np.linspace(0.9,1.01,npoints)
        self.fix_Er = np.zeros((self.CPZ.nt,npoints)) * np.nan
        fix_diaImp = np.zeros((self.CPZ.nt,npoints)) * np.nan
        fix_vpolBt = np.zeros((self.CPZ.nt,npoints)) * np.nan
        fix_vtorBp = np.zeros((self.CPZ.nt,npoints)) * np.nan
        fix_diaEl = np.zeros((self.CPZ.nt,npoints)) * np.nan
        for jt in range(self.CPZ.nt):
            self.fix_Er[jt] = interp.extrap(self.fix_grid,self.sf_rhoPol[jt],self.sf_Er[jt])
            fix_diaImp[jt] = interp.extrap(self.fix_grid,self.sf_rhoPol[jt],self.sf_diaImp[jt])
            fix_vpolBt[jt] = interp.extrap(self.fix_grid,self.sf_rhoPol[jt],self.sf_vpol[jt]*self.sf_Bt[jt])
            fix_vtorBp[jt] = interp.extrap(self.fix_grid,self.sf_rhoPol[jt],self.sf_vtor[jt]*self.sf_Bp[jt])
            fix_diaEl[jt] = interp.extrap(self.fix_grid,self.sf_rhoPol[jt],self.sf_diaEl[jt])
        if plot or logger.level <= 20:
            for jt in range(self.CMZ.nt):
                f = plt.figure()
                OnClick.Click2Zoom(f)
                ax = f.add_subplot(111)
                ax.plot(self.fix_grid,self.fix_Er[jt],label='$E_r$')
                ax.plot(self.fix_grid,fix_diaImp[jt],label='DiaImp')
                ax.plot(self.fix_grid,-fix_vpolBt[jt],\
                        label='$-v_{pol}*B_{tor}$')
                ax.plot(self.fix_grid,fix_vtorBp[jt],\
                        label='$v_{tor}*B_{pol}$')
                ax.plot(self.fix_grid,fix_diaEl[jt],label='DiaEl')
                ax.legend()
                plt.show()
        if time_trace:
            rho_index = np.where(self.fix_grid == self.fix_grid[np.argmin(np.abs(rho_plot-self.fix_grid))])[0]
            rho_index = rho_index[0]
            f = plt.figure()
            OnClick.Click2Zoom(f)
            ax = f.add_subplot(111)
            ax.plot(self.CPZ.time,self.fix_Er[:,rho_index],label='$E_r$')
            ax.legend()
            plt.show()
            if logger.level <= 10:
                ipsh()

    def check_alignment_ions_electrons(self,statistics=False):
        self.min_ti = np.zeros(self.CPZ.nt)*np.nan
        self.min_te = np.zeros(self.CPZ.nt)*np.nan
        self.delta_ti_te = np.zeros(self.CPZ.nt)*np.nan
        for jtCPZ in range(self.CPZ.nt):
            try:
                gridIndex = np.arange(self.CPZ.ngrid)\
                    [(self.CPZ.gridRho[jtCPZ] >= 0.85)\
                    *(self.CPZ.gridRho[jtCPZ] <= 0.99)\
                    *(self.Ti[jtCPZ](self.CPZ.gridR[jtCPZ])>0.0)]
                grad_ti = self.Ti[jtCPZ](self.CPZ.gridR[jtCPZ,gridIndex],nu=1)
                x = self.CPZ.gridRho[jtCPZ,gridIndex]
                min_ti = x[np.where(grad_ti == np.min(grad_ti))]
                jtIDA = np.where(self.IDA.time == self.IDA.time[np.argmin(np.abs(self.IDA.time\
                             -self.CPZ.time[jtCPZ]))])[0]
                jtIDA=jtIDA[0]
                gridIndexIDA = np.arange(np.size(self.IDA.ne.area.dat[jtIDA]))\
                    [(self.IDA.Te.area.dat[jtIDA] >= 0.85)\
                    *(self.IDA.Te.area.dat[jtIDA] <= 0.99)\
                    *(self.IDA.Te.dat[jtIDA] > 0)]
                grad_Te = self.IDA.dTe_dr.dat[jtIDA,gridIndexIDA]
                x = self.IDA.Te.area.dat[jtIDA,gridIndexIDA]
                min_te = x[np.where(grad_Te == np.min(grad_Te))]
                self.delta_ti_te[jtCPZ] = min_ti-min_te
            except:
                self.delta_ti_te[jtCPZ] = np.nan
                continue
        nan_indexes = np.where(np.isnan(self.delta_ti_te))
        self.delta_ti_te = np.delete(self.delta_ti_te,nan_indexes)
        if statistics:
            self.mean_delta_ti_te = np.mean(self.delta_ti_te)
            self.std_delta_ti_te = np.std(self.delta_ti_te)
