# Reference RW means book of Rasmussen&Williams http://www.gaussianprocess.org/gpml/chapters/RW.pdf

# %%
import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import gpflow
from scipy.optimize import fmin, minimize
from EER.lib import gp_functions, Ipsh

class FitGpFlow:

    def __init__(self,x,y,dy=None,lengthscale=1.,stdfit=None):
        self.xtrain = x.reshape(-1,1)     # Reshape due to convention
        self.ytrain = y.reshape(-1,1)

        # optimizable hyperparameters
        self.l = lengthscale         # length scale
        
        if stdfit is None:
            print("You need to provide a standard fit")
        self.stdfit = stdfit

        k = gpflow.kernels.SquaredExponential(variance=20.0**2, lengthscales=self.l)
        self.m = gpflow.models.GPR((self.xtrain, self.ytrain/self.noise_scale(self.xtrain)), k, noise_variance=1.0)
        gpflow.utilities.set_trainable(self.m.kernel.lengthscales, False)
        gpflow.utilities.set_trainable(self.m.kernel.variance, False)

        opt = gpflow.optimizers.Scipy()
        opt_logs = opt.minimize(self.m.training_loss, self.m.trainable_variables, options=dict(maxiter=100))
        gpflow.utilities.print_summary(self.m)

    def noise_scale(self,x):
        ret = np.empty_like(x)
        ret = self.stdfit(x)
        ret[x < self.xtrain[0]] = self.stdfit(self.xtrain[0])
        ret[x > self.xtrain[-1]] = self.stdfit(self.xtrain[-1])
        return ret

        k = gpflow.kernels.SquaredExponential(variance=20.0**2, lengthscales=l)
        m = gpflow.models.GPR((xtrain, ytrain/noise_scale(xtrain)), k, noise_variance=1.0)
        gpflow.utilities.set_trainable(m.kernel.lengthscales, False)
        gpflow.utilities.set_trainable(m.kernel.variance, False)

        opt = gpflow.optimizers.Scipy()
        opt_logs = opt.minimize(m.training_loss, m.trainable_variables, options=dict(maxiter=100))

    def __call__(self,xtest,nu=0):
        xtest = np.double(xtest.reshape(-1,1))

        if nu == 0:
            Ef, varf = self.m.predict_f(xtest)
            Ef = Ef*self.noise_scale(xtest)
            varf = varf*self.noise_scale(xtest)**2
            d67f = np.sqrt(varf)
            d67y = np.sqrt(varf + self.m.likelihood.variance.numpy()*self.noise_scale(xtest)**2)
            return np.array(Ef).flatten(),np.array(d67y).flatten()

        if nu == 1:
            Edfdx, vardfdx = gp_functions.predict_dfdx(
                [self.l, self.m.likelihood.variance.numpy()/self.m.kernel.variance.numpy()], 
                self.xtrain, self.ytrain, xtest)
            Edfdx*self.noise_scale(xtest)
            vardfdx = self.m.kernel.variance.numpy()*vardfdx*self.noise_scale(xtest)**2
            d67dfdx = np.sqrt(np.diag(vardfdx))
            return np.array(Edfdx).flatten(),np.array(d67dfdx).flatten()

    def plot(self,ax=None,nu=0):
        if ax is None:
            f = plt.figure()
            ax = f.add_subplot(111)
        if nu == 0:
            xtest = np.linspace(self.xtrain.min(),self.xtrain.max(),100)
            Ef,d67y = self.__call__(xtest)
            d95y = 1.96*d67y

            ax.plot(self.xtrain, self.ytrain, 'ko')
            # Total variance
            ax.fill_between(xtest, 
                tf.reshape(Ef - d95y, [-1]),
                tf.reshape(Ef + d95y, [-1]),
                alpha=0.5)

            ax.plot(xtest, Ef, 'ko',label='GP')
        if nu == 1:
            Edfdx,d67dfdx = self.__call__(xtest,nu=1)
            print(Edfdx.shape,d67dfdx.shape)
            d95dfdx = 1.96*d67dfdx
            # Fit variance
            ax.fill_between(xtest, 
                (Edfdx - d95dfdx),
                (Edfdx + d95dfdx),
                alpha=0.5, color='tab:red')
            ax.plot(xtest, Edfdx, 'k-',label='GP')
