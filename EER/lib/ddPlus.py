import matplotlib.pylab as plt
import warnings
warnings.simplefilter('always', DeprecationWarning)
import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
import dd
from copy import copy
import numpy as np
import logging, re
from scipy.signal import *

class shotfile(dd.shotfile):
    def __init__(self, diagnostic=None, pulseNumber=None, \
                 experiment='AUGD', edition=0):
        self.diagnostic = diagnostic
        self.pulseNumber = pulseNumber
        self.experiment = experiment
        self.edition = edition
        super(shotfile, self).__init__(diagnostic=diagnostic,\
                                  pulseNumber=pulseNumber,\
                                  experiment=experiment,\
                                  edition=edition)

    def __call__(self, name, dtype=None, tBegin=None, \
                 tEnd=None, calibrated=True, index=None):
        output = super(shotfile, self).__call__(name,\
                       dtype=dtype,tBegin=tBegin, \
                       tEnd=tEnd, calibrated=calibrated, index=index)
        # Use subclass of signal and signal group instead of 
        # basic classes
        if type(output) == dd.signalGroup:
            return signalGroup.fromBaseSignalGroup(output,self.shot)
        if type(output) == dd.signal:
            return signal.fromBaseSignal(output,self.shot)
        return output

    def readAll(self):
        """
        Read all signals and signal groups in the shotfile
        and assign as class attributes (non alphanumeric chars are removed)
        """
        signalList = self.getSignalNames()
        self.signalList = []
        for signal in signalList:
            logging.debug('Reading signal '+signal)
            try:
                attributeName = re.sub(r'\W+', '', signal)
                setattr(self,attributeName,self(signal))
                self.signalList.append(signal)
            except:
                logging.warning(signal+' not read!')
        signalGroupList = self.getSignalGroupNames()
        self.signalGroupList = []
        for signal in signalGroupList:
            logging.debug('Reading signal group '+signal)
            try:
                attributeName = re.sub(r'\W+', '', signal)
                setattr(self,attributeName,self(signal))
                self.signalGroupList.append(signal)
            except:
                logging.warning(signal+' not read!')

class signal(dd.signal):
    def __init__(self, name=None, header=None, data=None, shot=None, time=None, unit='', area=None):
        self.shot = shot
        super(signal, self).__init__(name,header,data,\
                                       time=time,unit=unit,area=area)

    @classmethod
    def fromBaseSignal(cls, obj, shot):
        obj.shot = shot
        obj.__class__ = signal
        return obj

    def spectrogram(self,plot=False,**spectrokwargs):
        from utils import Spectrogram
        self.spec = Spectrogram.Spectrogram(\
                     self.data,self.time,**spectrokwargs)
        self.spec.stft()
        if plot:
            self.spec.plot()
        nt = self.spec.spectrogram.shape[1]
        timeBase = np.linspace(self.time[0],self.time[-1],num=nt)
        output = signalGroup(data=self.spec.spectrogram.T,time=timeBase,\
                             shot=self.shot)
        #create "fake" areaBase
        area = dd.areaBase(None,None,self.spec.fspectrogram)
        output.area = area
        return output

    def conditionalSynchronization(self,timeBaseSync=np.array([]),ShotELM=None,\
                                   ExpELM='AUGD',EdELM=0,full_output=False):
        from utils import Utils
        if timeBaseSync.size == 0:
            if ShotELM == None:
                elm = shotfile('ELM',self.shot,ExpELM,EdELM)
            else:
                elm = shotfile('ELM',ShotELM,ExpELM,EdELM)
            timeBaseSync = elm('t_begELM')
        NewTime,NewData = Utils.ConditionalSynchronization(self.time,\
                                                     self.data,\
                                                     timeBaseSync)
        timeBaseSync = elm('t_begELM')
        index = np.arange(timeBaseSync.size)[
                (timeBaseSync>self.time.min())*(timeBaseSync<self.time.max())]
        timeBaseSync = timeBaseSync[index]
        SyncAttributes = {}
        output = signal(self.name, self.header,\
                      NewData, self.shot, NewTime, self.unit)
        if full_output:
            return output,timeBaseSync
        return output

    def bin(self,dt,full_output=False):
        from utils import Utils
        if type(dt) == np.int:
            dtIndex = dt
        else:
            meanTimeResolution = np.average(np.diff(self.time))
            dtIndex = np.int(np.round(dt/meanTimeResolution,0))
        if dtIndex <= 1:
            return signal(self.name, self.header, self.data, self.shot,\
                           self.time, self.unit)
        if self.time[0]*self.time[-1] < 0.:
            j0 = np.argmin(np.abs(self.time))
            timeNeg = Utils.Bin(self.time[:j0][::-1],dtIndex)[::-1]
            timePos = Utils.Bin(self.time[j0:],dtIndex)
            binTime = np.append(timeNeg,timePos)
            dataNeg,stdNeg = Utils.Bin(self.data[:j0][::-1],dtIndex,std=True)
            dataNeg = dataNeg[::-1]
            stdNeg = stdNeg[::-1]
            dataPos,stdPos = Utils.Bin(self.data[j0:],dtIndex,std=True)
            binData = np.append(dataNeg,dataPos)
            binStd = np.append(stdNeg,stdPos)
        else:
            binTime = Utils.Bin(self.time,dtIndex)
            binData,binStd = Utils.Bin(self.data,dtIndex,std=True)
        signalTmp = signal(self.name, self.header, binData, self.shot,\
                           binTime, self.unit)
        signalStd = signal(self.name, self.header, binStd, self.shot,\
                           binTime, self.unit)
        if full_output:
            return signalTmp,signalStd
        return signalTmp

    def __call__(self, tBegin=False, tEnd=False, index=False, timeRanges=None):
        if timeRanges != None:
            timeRanges = np.atleast_2d(timeRanges)
            output = self.__call__(timeRanges[0,0],timeRanges[0,1])
            for range in timeRanges[1:]:
                output = output.append(self.__call__(range[0],range[1]))
            return output
        if tBegin == tEnd:
            index = np.argmin(np.abs(self.time - tBegin))
        else:
            index = np.arange(self.time.size)\
                    [(self.time >= tBegin)*(self.time <= tEnd)]
        output = signal(self.name, self.header,\
                      self.data[index], self.shot, self.time[index], self.unit)
        return output

    def append(self,s):
        area = copy(self.area)
        newData = np.append(self.data,s.data,axis=0)
        newTime = np.append(self.time,s.time,axis=0)
        output = signal(self.name, self.header, newData, self.shot,\
                           newTime, self.unit)
        return output

    def movingAverage(self,dtIndex):
        from utils import Utils
        if dtIndex < 1:
            return signal(self.name, self.header, self.data, self.shot,\
                          self.time, self.unit)
        binData = Utils.CentralMovingAverage(self.data,n=dtIndex)
        binTime = Utils.CentralMovingAverage(self.time,n=dtIndex)
        index = np.where(~np.isnan(binTime))
        binData = binData[index]
        binTime = binTime[index]
        binArea = copy(self.area)
        signalTmp = signalGroup(self.name, self.header, binData,\
                                self.shot, binTime, self.unit)
        return signalTmp

    def plot(self,ax=False,scale=1.,offset=0.,
             downsampling=1,**kwargsplot):
        show = False
        if ax:
            pass
        else:
            f = plt.figure()
            ax = f.add_subplot(111)
            show = True
        if scale == 0.:
            expScale = np.round(np.log10(np.max(np.abs(self.data))),0)-1
            scale = 10**(-expScale)
        ax.plot(self.time[::downsampling],\
                self.data[::downsampling]*scale+offset,**kwargsplot)
        ax.set_xlabel('Time [s]')
        try:
            ax.set_ylabel(self.header.name+(' [%.0E '%scale)+self.unit+']')
        except:
            pass
        if show:
            plt.show()

    def crossCorrelation(self,signalInput):
        from utils import correlation
        # resample input signal on timebase
        from scipy.interpolate import interp1d
        intfunc = interp1d(signalInput.time,signalInput.data)
        signalInterp = intfunc(self.time)
        dt = np.average(np.diff(self.time))
        if ((np.diff(self.time) - dt) > dt).any():
            raise ValueError('Sampling rate has to be constant')
        cor, tau = correlation.xcor(self.data,signalInterp,dt,\
                                    norm=True)
        return signal(self.name,self.header,\
                      cor, self.shot, tau, self.unit)

    def log(self):
        return signal(self.name, self.header,\
                      np.log(self.data), self.shot, self.time, self.unit)

    def log10(self):
        return signal(self.name, self.header,\
                      np.log10(self.data), self.shot, self.time, self.unit)

    def highPassFilter(self,cutOffF,transitionBand=0.08):
        fc = cutOffF
        b = transitionBand
        N = int(np.ceil((4 / b)))
        if not N % 2: N += 1  # Make sure that N is odd.
        n = np.arange(N)

        # Compute a low-pass filter.
        h = np.sinc(2 * fc * (n - (N - 1) / 2.))
        w = np.blackman(N)
        h = h * w
        h = h / np.sum(h)

        # Create a high-pass filter from the low-pass filter through spectral inversion.
        h = -h
        h[(N - 1) / 2] += 1
        return signal(self.name, self.header,\
                  np.convolve(self.data,h,'same'), self.shot, self.time, self.unit)

    def lowPassFilter(self,cutOffF,transitionBand=0.08):
        fc = cutOffF
        b = transitionBand
        N = int(np.ceil((4 / b)))
        if not N % 2: N += 1  # Make sure that N is odd.
        n = np.arange(N)

        # Compute a low-pass filter.
        h = np.sinc(2 * fc * (n - (N - 1) / 2.))
        w = np.blackman(N)
        h = h * w
        h = h / np.sum(h)
        return signal(self.name, self.header,\
                  np.convolve(self.data,h,'same'), self.shot, self.time, self.unit)

    def iirfilter(self,fpass,fstop,gstop=10,gpass=1,ftype='cheby2',plot=False):
        fnquist = 1./(np.average(np.diff(self.time))/2.)
        fstop = fstop/fnquist
        fpass = fpass/fnquist
        #Generate filter
        b, a = iirdesign(wp=fpass, ws=fstop, gstop=gstop,\
                         gpass=gpass, ftype=ftype)
        if plot:
            w, h = freqs(b, a)
            plt.semilogx(w, 20 * np.log10(abs(h)))
            plt.title('Chebyshev Type I frequency response (rp=5)')
            plt.xlabel('Frequency [radians / second]')
            plt.ylabel('Amplitude [dB]')
            plt.margins(0, 0.1)
            plt.grid(which='both', axis='both')
            plt.show()
        output_signal = filtfilt(b, a, self.data)
        return signal(self.name, self.header,\
                  output_signal, self.shot, self.time, self.unit)

class signalGroup(dd.signalGroup):
    def __init__(self, name=None, header=None, data=None, shot=None, time=None, unit='', area=None):
        self.shot = shot
        super(signalGroup, self).__init__(name,header,data,\
                                       time=time,unit=unit,area=area)

    @classmethod
    def fromBaseSignalGroup(cls, obj, shot):
        obj.shot = shot
        obj.__class__ = signalGroup
        return obj

    def conditionalSynchronization(self,timeBaseSync=np.array([]),ShotELM=None,\
                                  ExpELM='AUGD',EdELM=0,full_output=False):
        from utils import Utils
        if timeBaseSync.size == 0:
            if ShotELM == None:
                elm = shotfile('ELM',self.shot,ExpELM,EdELM)
            else:
                elm = shotfile('ELM',ShotELM,ExpELM,EdELM)
            timeBaseSync = elm('t_begELM')
        NewTime,NewData = \
                Utils.ConditionalSynchronization(self.time,\
                                                 self.data,\
                                                 timeBaseSync)
        area = copy(self.area)
        try:
            if self.area.data.shape[0] == self.time.size:
                NewTime,NewArea = Utils.ConditionalSynchronization(self.time,\
                                                             self.area.data,\
                                                             timeBaseSync)
                area.data = NewArea
        except:
            pass
        out = signalGroup(self.name, self.header,\
                      NewData, self.shot, NewTime, self.unit, area)
        if full_output:
            return out,timeBaseSync
        return out

    def bin(self,dt,full_output=False):
        from utils import Utils
        if type(dt) == np.int:
            dtIndex = dt
        else:
            meanTimeResolution = np.average(np.diff(self.time))
            dtIndex = np.int(np.round(dt/meanTimeResolution,0))
        if dtIndex <= 1:
            if full_output:
                return signalGroup(self.name, self.header, self.data, self.shot,\
                               self.time, self.unit, self.area),\
                       signalGroup(self.name, self.header, self.data*0., self.shot,\
                                   self.time, self.unit, self.area)
            else:
                return signalGroup(self.name, self.header, self.data, self.shot,\
                               self.time, self.unit, self.area)
        binArea = copy(self.area)
        binAreaStd = copy(self.area)
        #Bin Around time = 0. if time crosses 0.
        if self.time[0]*self.time[-1] < 0.:
            j0 = np.argmin(np.abs(self.time))
            timeNeg = Utils.Bin(self.time[:j0][::-1],dtIndex)[::-1]
            timePos = Utils.Bin(self.time[j0:],dtIndex)
            binTime = np.append(timeNeg,timePos)
            dataNeg,stdNeg = Utils.Bin(self.data[:j0][::-1],dtIndex,std=True)
            dataPos,stdPos = Utils.Bin(self.data[j0:],dtIndex,std=True)
            binData = np.append(dataNeg[::-1],dataPos,axis=0)
            binStd = np.append(stdNeg[::-1],stdPos,axis=0)
            try:
                if self.area.data.shape[0] == self.time.size:
                    areaNeg,stdAreaNeg = Utils.Bin(self.area.data[:j0][::-1],dtIndex,std=True)
                    areaPos,stdAreaPos = Utils.Bin(self.area.data[j0:],dtIndex,std=True)
                    binArea.data = np.append(areaNeg[::-1],areaPos,axis=0)
                    binAreaStd.data = np.append(stdAreaNeg[::-1],stdAreaPos,axis=0)
            except:
                pass
        else:
            binTime = Utils.Bin(self.time,dtIndex)
            binData,binStd = Utils.Bin(self.data,dtIndex,std=True)
            try:
                if self.area.data.shape[0] == self.time.size:
                    binArea.data,binAreaStd.data = Utils.Bin(self.area.data,\
                                                     dtIndex,std=True)
            except:
                pass
        signalTmp = signalGroup(self.name, self.header, binData, self.shot,\
                           binTime, self.unit, binArea)
        signalStdTmp = signalGroup(self.name, self.header, binStd, self.shot,\
                           binTime, self.unit, binArea)
        if full_output:
            return signalTmp,signalStdTmp
        return signalTmp

    def iirfilter(self,fpass,fstop,gstop=10,gpass=1,ftype='cheby2',plot=False):
        fnquist = 1./(np.average(np.diff(self.time))/2.)
        fstop = fstop/fnquist
        fpass = fpass/fnquist
        #Generate filter
        b, a = iirdesign(wp=fpass, ws=fstop, gstop=gstop,\
                         gpass=gpass, ftype=ftype)
        if plot:
            w, h = freqs(b, a)
            plt.semilogx(w, 20 * np.log10(abs(h)))
            plt.title('Chebyshev Type I frequency response (rp=5)')
            plt.xlabel('Frequency [radians / second]')
            plt.ylabel('Amplitude [dB]')
            plt.margins(0, 0.1)
            plt.grid(which='both', axis='both')
            plt.show()
        newData = self.data*0.
        for jch in xrange(self.data.shape[1]):
            newData[:,jch] = filtfilt(b, a, self.data[:,jch])
        signalTmp = signalGroup(self.name, self.header, newData, self.shot,\
                           self.time, self.unit, self.area)
        return signalTmp

    def __call__(self, tBegin=False, tEnd=False, index=False, timeRanges=None):
        if timeRanges != None:
            timeRanges = np.atleast_2d(timeRanges)
            output = self.__call__(timeRanges[0,0],timeRanges[0,1])
            for range in timeRanges[1:]:
                output = output.append(self.__call__(range[0],range[1]))
            return output
        if self.time is None:
            raise Exception('SignalGroup is not time dependent.')
        if not tBegin and not tEnd:
            try:
                if not index:
                    index = np.arange(self.time.size)
            except:
                pass
        elif tBegin == tEnd:
            index = np.argmin(np.abs(self.time - tBegin))
        else:
            index = np.arange(self.time.size)[(self.time >= tBegin)*(self.time <= tEnd)]
        area = copy(self.area)
        try:
            area.data = area.data[index]
        except:
            pass
        output = signalGroup(self.name, self.header, self.data[index], self.shot,\
                           self.time[index], self.unit, area)
        return output

    def append(self,sG):
        area = copy(self.area)
        try:
            if area.data.shape[0] == self.time.size and \
               sG.area.data.shape[0] == sG.time.size:
                area = np.append(area.data,sG.area.data,axis=0)
        except:
            pass
        newData = np.append(self.data,sG.data,axis=0)
        newTime = np.append(self.time,sG.time,axis=0)
        output = signalGroup(self.name, self.header, newData, self.shot,\
                           newTime, self.unit, area)
        return output

    def movingAverage(self,dtIndex):
        from utils import Utils
        if dtIndex < 1:
            return signalGroup(self.name, self.header, self.data, self.shot,\
                           self.time, self.unit, self.area)
        binData = Utils.CentralMovingAverage(self.data,n=dtIndex)
        binTime = Utils.CentralMovingAverage(self.time,n=dtIndex)
        index = np.where(~np.isnan(binTime))
        binData = binData[index]
        binTime = binTime[index]
        binArea = copy(self.area)
        try:
            if self.area.data.shape[0] == self.time.size:
                binArea.data = Utils.CentralMovingAverage(\
                                                self.area.data,\
                                                n=dtIndex)
                binArea.data = binArea.data[index]
        except:
            pass
        signalTmp = signalGroup(self.name, self.header, binData,\
                                self.shot, binTime, self.unit, binArea)
        return signalTmp

    def mean(self):
        output = signalGroup(self.name, self.header, np.mean(self.data,axis=0), self.shot,\
                           np.mean(self.time), self.unit, self.area)
        return output

    def plot(self,contour=False,ax=False,scale=0.,offset=0.,\
             channels=np.array([]),errorbar=np.array([]),**kwargsplot):
        from utils import Style
        show = False
        if ax:
            pass
        else:
            f = plt.figure()
            ax = f.add_subplot(111)
            show = True
        if scale == 0.:
            expScale = np.round(np.log10(np.max(np.abs(self.data))),0)-1
            scale = 10**(-expScale)
        if not contour:
            if channels.size == 0.:
                channels = np.arange(self.data.shape[1])
            colors = Style.colors(channels.size)
            jcol = 0
            for jch in channels:
                label = None
                try:
                    if not self.area.data.shape[0] == self.time.size:
                        label = self.area[jch]
                except:
                    pass
                if errorbar.size == 0:
                    ax.plot(self.time,self.data[:,jch]*scale+offset,\
                            label=label,color=colors[jcol],**kwargsplot)
                else:
                    ax.errorbar(self.time,self.data[:,jch]*scale+offset,\
                            errorbar.data[:,jch]*scale+offset,\
                            label=label,color=colors[jcol],**kwargsplot)
                jcol += 1
        else:
            if self.area == None:
                ax.imshow(self.data.T*scale,extent=[self.time[0],self.time[-1],\
                          1.,self.data.shape[1]],interpolation=None,\
                          origin='lower', aspect='auto', **kwargsplot)
            elif self.area.shape[0] == self.time.size:
                mesh = ax.pcolormesh(\
                            np.tile(self.time,(self.area.data.shape[1],1)).T,\
                            self.area.data,self.data*scale+offset,\
                            **kwargsplot)
                cbar = plt.colorbar(mesh,ax=ax,format='%05.2f')
                cbar.connect()
            else:
                mesh = ax.pcolor(self.time, self.area.data,\
                          self.data.T*scale+offset, **kwargsplot)
                cbar = plt.colorbar(mesh,ax=ax,format='%05.2f')
                cbar.connect()
        ax.set_xlabel('Time [s]')
        try:
            ax.set_ylabel(self.header.name+(' [%.0E '%scale)+self.unit+']')
        except:
            pass
        ax.legend(loc='best')
        if show:
            plt.show()

if __name__ == '__main__':
    from scipy import ndimage
    from utils import Style
    Style.Pub(width=400,fontsize=12)
    mhe = shotfile('MHA',31529)
    b31 = mhe('B31-14')(4.0,5.5)
    b31 = b31.spectrogram(fftFrame=np.int(2048),hop=np.int(512))
    b31 = b31.conditionalSynchronization(EdELM=10,ExpELM='MCAVEDON')
    b31 = b31.bin(80.e-6)
    b31.data = ndimage.rotate(b31.data, 90)
    plt.imshow(b31.data,aspect='auto',extent=[b31.time.min()*1.e3,b31.time.max()*1.e3,\
                                             b31.area.data.min()/1.e3,b31.area.data.max()/1.e3],vmax=2.8,vmin=0.5)
    #plt.imshow(b31.data,aspect='auto',extent=[b31.time.min(),b31.time.max(),\
                                             #b31.area.data.min()/1.e3,b31.area.data.max()/1.e3],vmax=2.8,vmin=0.5)
    plt.ylim([0,400])
    plt.xlabel(r'$\rm time \, [ms]$')
    plt.ylabel(r'$\nu \, \rm [kHz]$')
    plt.title(r'$\# 31529$',loc='right')
    plt.title(r'$\rm B31$-$14$',loc='left')
    plt.xlim([-5,10])
    plt.tight_layout()
    #plt.savefig('/afs/ipp/u/mcavedon/tex/2016_ELM_meeting/images/SpecSync.pdf')
    plt.show()
