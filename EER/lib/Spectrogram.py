"""
Class for calculating spectrogram of 1D signal using FFT
(it works only for constantly sampled signals)

to do:
    - implement for wavelet transform

"""
__author__ = 'mcavedon@ipp.mpg.de'
__date__ = '10.12.14'
__version__ = '0.2'

import numpy as np
import matplotlib.pylab as plt
import logging

class Spectrogram:
    def __init__(self, signal, timeBase, 
                 fftFrame=0.01, hop=0.003, force=False, \
                 largestscale=2, notes=0, scaling='log'):
        """
        Initialization of spectrogram:
            Input:
                - signal   -> input data
                - timeBase -> time base of input signal
                - fftFrame -> time window in seconds of taken for the fft
                              (it is by default converted in the closest power of
                               2 for improving performance if keyword force=False).
                               If integer is given it is use directly as fft interval.
                - hop      -> hopping step in seconds (if int taken as value)
            Optional keyword:
                - force    -> force to take exactly input fftFrame (really slow!!!!)
        Note:
            Default values are a good setting for Mirnov coil
        """
        self.signal = signal
        self.timeBase = timeBase
        self.fs = 1./np.average(np.diff(self.timeBase)) #average sampling frequency
        self.fftFrame_s = fftFrame
        self.fftFrame_i = int(2**np.ceil(np.log2(self.fftFrame_s*self.fs))) # converted fftFrame to index
        if type(fftFrame) == np.int:
            self.fftFrame_i = fftFrame
        if force:
            self.fftFrame_i = int(self.fftFrame_s*self.fs)
        self.hop_s = hop
        self.hop_i = int(self.hop_s*self.fs)
        if type(hop) == np.int:
            self.hop_i = hop
        logging.info('Nfft %d, Nstep %d'%(self.fftFrame_i,self.hop_i))
        self.largestscale = largestscale
        self.notes = notes
        self.scaling = scaling

    def stft(self):
        """
            Short-time Fourier transform with Hann windowing
        """
        w = np.hanning(self.fftFrame_i+1)[:-1]
        ## better reconstruction with this trick +1)[:-1]  
        self.X =  np.array([np.fft.rfft(w*self.signal[i:i+self.fftFrame_i]) \
                         for i in range(0, len(self.signal)-self.fftFrame_i, self.hop_i)])
        n,m = self.X.shape
        self.fX = np.linspace(0.,self.fs//2,m) #Frequency axis
        self.spectrogram = np.log10(np.absolute(self.X.T))
        self.fspectrogram = self.fX

    def istft(self):
        """
            Inverse of short-time Fourier transform with Hann windowing
        """
        fftsize=(self.X.shape[1]-1)*2
        w = np.hanning(fftsize+1)[:-1]
        self.outputSignal = np.zeros(self.X.shape[0]*self.hop_i)
        wsum = np.zeros(self.X.shape[0]*self.hop_i)
        for n,i in enumerate(range(0, len(self.outputSignal)-fftsize, self.hop_i)):
            self.outputSignal[i:i+fftsize] += np.real(np.fft.irfft(self.X[n])) * w   # overlap-add
            wsum[i:i+fftsize] += w ** 2.
        pos = wsum != 0
        self.outputSignal[pos] /= wsum[pos]

    def plot(self,axis=None,fmax=None,downsampling=1,**imshowkwargs):
        if fmax != None:
            ind = np.where(self.fX < fmax)
            self.spectrogram = self.spectrogram[ind]
            self.fspectrogram = self.fspectrogram[ind]
        if axis == None:
            f = plt.figure()
            axis = f.add_subplot(111)

        self.img = axis.imshow(self.spectrogram[::downsampling], origin='lower', aspect='auto',
                          interpolation=None,\
                          extent=[self.timeBase[0],self.timeBase[-1],\
                                  self.fspectrogram.min()/1e3,self.fspectrogram.max()/1e3],
                        **imshowkwargs)
        axis.set_ylabel('Frequency [kHz]')

if __name__ == '__main__':
    from dd import dd
    from Debug.Ipsh import *
    cnz = dd.shotfile('CNZ',32758,edition=0,experiment='AUGD')
    mha = dd.shotfile('MHA',32758,edition=0,experiment='AUGD')
    tbeg = 3.65
    tend = 4.0
    b13 = mha('B31-13')(tbeg,tend)
    inte = cnz('Ti_c')(tbeg,tend)
    spe2 = Spectrogram(b13.data,b13.time)
    spe = Spectrogram(inte.data[:,6],inte.time)
    f = plt.figure()
    ax1 = f.add_subplot(211)
    ax2 = f.add_subplot(212)
    spe2.stft()
    spe2.plot(fmax=5e3,cmap='bwr',axis=ax1)
    cbar2 = f.colorbar(spe2.img,ax=ax2)
    spe.stft()
    spe.plot(fmax=3e3,cmap='bwr',axis=ax2, vmin=1, vmax=3)
    cbar = f.colorbar(spe.img,ax=ax1)
    indexes = np.where(np.diff(inte.time)>100e-6)[0]
    ax2.axvspan(0,inte.time[0],color='lightgray')
    for jbeg in indexes:
        ax2.axvspan(inte.time[jbeg],inte.time[jbeg+1],color='lightgray')
    ax2.set_yticks([0,0.6,1.2,1.8,2.4,3.0])
    ax1.set_xlim([tbeg,tend])
    ax2.set_xlim([tbeg,tend])
    ax1.set_xticklabels([])
    #ax2.plot(inte.time,inte.time*0.+1,'o')
    ax2.set_yticklabels([0,1,2,3,4])
    ax1.set_ylabel('inte [ph/...]')
    ax2.set_xlabel('time [s]')
    plt.show()
