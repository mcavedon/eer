import numpy as np
from scipy.interpolate import UnivariateSpline, splev, splrep
from scipy.optimize import minimize

def guess(x, y, k, s, w=None):
    """Do an ordinary spline fit to provide knots"""
    return splrep(x, y, w, k=k, s=s)

def err(c, x, y, t, k, w=None):
    """The error function to minimize"""
    diff = y - splev(x, (t, c, k))
    if w is None:
        diff = np.einsum('...i,...i', diff, diff)
    else:
        diff = np.dot(diff*diff, w)
    return np.abs(diff)

def prior(c, x, t, k, p):
    return splev(x, (t, c, k), der=2).sum()/p

def err_prior(c, x, y, t, k, p, w=None):
    """The error function to minimize"""
    diff = y - splev(x, (t, c, k))
    if w is None:
        diff = np.einsum('...i,...i', diff, diff)
    else:
        diff = np.dot(diff*diff, w)
    return np.abs(diff)+np.abs(prior(c, x, t, k, p))

def spline(x, y, p=1, k=3, s=0, w=None):
    t, c0, k = guess(x, y, k, s, w=w)
    opt = minimize(err_prior, c0, (x, y, t, k, p, w))
    copt = opt.x
    return UnivariateSpline._from_tck((t, copt, k))

def spline_negative_derivative_curvature(x, y, p=1, k=3, s=0, w=None):
    t, c0, k = guess(x, y, k, s, w=w)
    con = {'type': 'ineq',
           'fun': lambda c: -splev(x, (t, c, k), der=1),
           }
    opt = minimize(err_prior, c0, (x, y, t, k, p, w), constraints=con)
    copt = opt.x
    return UnivariateSpline._from_tck((t, copt, k))

def spline_negative_derivative_null_center(x, y, k=3, s=0, w=None):
    t, c0, k = guess(x, y, k, s, w=w)
    x0 = x[0]
    con = [{'type': 'ineq',
           'fun': lambda c: -splev(x, (t, c, k), der=1),
           },\
          {'type': 'eq',
           'fun': lambda c: splev(x0, (t, c, k), der=1),
           }]
    opt = minimize(err, c0, (x, y, t, k, w), constraints=con)
    copt = opt.x
    return UnivariateSpline._from_tck((t, copt, k))

def spline_null_center(x, y, k=3, s=0, w=None):
    t, c0, k = guess(x, y, k, s, w=w)
    x0 = x[0]
    con = [{'type': 'eq',
           'fun': lambda c: splev(x0, (t, c, k), der=1),
           },]
    opt = minimize(err, c0, (x, y, t, k, w), constraints=con)
    copt = opt.x
    return UnivariateSpline._from_tck((t, copt, k))

def spline_negative_derivative(x, y, k=3, s=0, w=None):
    t, c0, k = guess(x, y, k, s, w=w)
    con = {'type': 'ineq',
           'fun': lambda c: -splev(x, (t, c, k), der=1),
          }
    opt = minimize(err, c0, (x, y, t, k, w), constraints=con)
    copt = opt.x
    return UnivariateSpline._from_tck((t, copt, k))

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    n = 10
    x = np.linspace(0, 2*np.pi, n)
    y0 = np.cos(x) # zero initial slope
    std = 0.5
    noise = np.random.normal(0, std, len(x))
    y = y0 + noise
    k = 3

    sp0 = UnivariateSpline(x, y, k=k, s=n*std)
    sp = spline_negative_derivative(x, y, k, s=n*std)

    plt.figure()
    X = np.linspace(x.min(), x.max(), len(x)*10)
    plt.plot(X, sp0(X), '-r', lw=1, label='guess')
    plt.plot(X, sp(X), '-r', lw=2, label='spline')
    plt.plot(X, sp.derivative()(X), '-g', label='slope')
    plt.plot(x, y, 'ok', label='data')
    plt.legend(loc='best')
    plt.show()
