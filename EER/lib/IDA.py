import numpy as np
import matplotlib.pylab as plt
import os
import EER.lib.ddPlus as ddPlus

class IDA(ddPlus.shotfile):
    def __init__(self, pulseNumber, diagnostic='IDA', experiment='AUGD', edition=0):
        super(IDA, self).__init__(diagnostic=diagnostic,\
                                  pulseNumber=pulseNumber,\
                                  experiment=experiment,\
                                  edition=edition)

    def getSignalList(self,signals=['ne','Te','dne_dr','dTe_dr'],**callkwargs):
        """
        Read list of signals and store as class attributes
        """
        self.profiles = signals
        for signal in signals:
            self.__dict__[signal.replace("-", "")] = \
                    self.__call__(signal,**callkwargs)
        self.time = self.__dict__[signal.replace("-", "")].time

    def selectTime(self, tBegin=False, tEnd=False, index=False):
        """
        Select time window
        """
        if not tBegin and not tEnd:
            pass
        elif tBegin == tEnd:
            index = np.argmin(np.abs(self.time - tBegin))
        else:
            index = np.arange(self.time.size)\
                    [(self.time >= tBegin)*(self.time <= tEnd)]
        output = IDA(self.pulseNumber,\
                     experiment=self.experiment,edition=self.edition)
        output.profiles = self.profiles
        output.time = np.atleast_1d(self.time[index])
        for pro in self.profiles:
            output.__dict__[pro] = self.__dict__[pro](index=index)
        return output
    
    def fluxCoordinate(self,ExpEq='AUGD',DiagEq='EQH',EdEq=0):
        from utils import kk
        kk = kk.KK()
        self.ndata = self.Te.data[0].size
        self.nt = self.time.size
        rMaj = np.zeros((self.nt,self.ndata))
        for jt in xrange(self.time.size):
            out = kk.kkrhorz(self.pulseNumber,self.time[jt],\
                             self.Te.area.data[jt],diag=DiagEq,exp=ExpEq,ed=EdEq)
            rMaj[jt] = out.r
        for pro in self.profiles:
            self.__dict__[pro].area.rMaj = rMaj

if __name__ == "__main__":
    from Debug import *
    ida = IDA(31689)
    ipsh()
