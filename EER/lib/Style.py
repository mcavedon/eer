from pylab import arange,pi,sin,cos,sqrt
import matplotlib.pylab as plt
from matplotlib.font_manager import FontProperties
import numpy as np
from matplotlib.ticker import ScalarFormatter,MaxNLocator
from math import log10, floor
from pylab import get_cmap
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.transforms import TransformedBbox, Affine2D


def PubNoLatex(height=0,width=372.,fontsize=10,pad=2):
  import brewer2mpl
  bmap = brewer2mpl.get_map('Set1', 'qualitative', 9)
  colors = bmap.mpl_colors
  plt.rcParams['axes.color_cycle'] = colors
  fig_width_pt=width# Get this from LaTeX using \showthe\columnwidth
  inches_per_pt = 1.0/72.27               # Convert pt to inch
  golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
  fig_width = fig_width_pt*inches_per_pt  # width in inches
  if height==0:
    fig_height=fig_width_pt*inches_per_pt*golden_mean    # height in inches
  else:
    fig_height=height*inches_per_pt
  fig_size =  [fig_width,fig_height]

  params = {'backend': 'pdf',
            'axes.labelsize'  :fontsize,
            'font.size'       :fontsize,
            'axes.titlesize'  :fontsize,
            'legend.fontsize' :fontsize-2,
            'xtick.labelsize' :fontsize-2,
            'ytick.labelsize' :fontsize-2,
            'xtick.major.pad':pad,
            'xtick.minor.pad':pad,
            'ytick.major.pad':pad,
            'ytick.minor.pad':pad,
            'figure.figsize'  :fig_size,
            #'font.family'     :'serif',
            'text.usetex'     :False,
            'axes.grid'       :False,
            'axes.linewidth'  :0.5,
            }
  plt.rcParams.update(params)

def Pub(height=0,width=372.,fontsize=10,pad=2):
  import brewer2mpl
  bmap = brewer2mpl.get_map('Set1', 'qualitative', 9)
  colors = bmap.mpl_colors
  plt.rcParams['axes.color_cycle'] = colors
  fig_width_pt=width# Get this from LaTeX using \showthe\columnwidth
  inches_per_pt = 1.0/72.27               # Convert pt to inch
  golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
  fig_width = fig_width_pt*inches_per_pt  # width in inches
  if height==0:
    fig_height=fig_width_pt*inches_per_pt*golden_mean    # height in inches
  else:
    fig_height=height*inches_per_pt
  fig_size =  [fig_width,fig_height]

  params = {'backend': 'pdf',
            'axes.labelsize'  :fontsize,
            'font.size'       :fontsize,
            'axes.titlesize'  :fontsize,
            'legend.fontsize' :fontsize-2,
            'xtick.labelsize' :fontsize-2,
            'ytick.labelsize' :fontsize-2,
            'xtick.major.pad':pad,
            'xtick.minor.pad':pad,
            'ytick.major.pad':pad,
            'ytick.minor.pad':pad,
            'figure.figsize'  :fig_size,
            'font.family'     :'serif',
            'text.usetex'     :True,
            'axes.grid'       :False,
            'axes.linewidth'  :0.5,
            }
  plt.rcParams.update(params)
  plt.rcParams['text.latex.preamble'] = [
       r'\usepackage{siunitx}',
       r'\usepackage{helvet}',
       r'\usepackage[usenames]{color}',
       r'\usepackage{bm}',
  ]

def Slide(height=0,width=372.,fontsize=15):
  #latex scale min=0.5
  #width=307.28987 beamer
  #width=600 for one page landscape figures
  fig_width_pt=width# Get this from LaTeX using \showthe\columnwidth
  inches_per_pt = 1.0/72.27               # Convert pt to inch
  golden_mean = (sqrt(5)-1.0)/2.0         # Aesthetic ratio
  fig_width = fig_width_pt*inches_per_pt  # width in inches
  if height==0:
    fig_height=fig_width_pt*inches_per_pt*golden_mean    # height in inches
  else:
    fig_height=height*inches_per_pt
  fig_size =  [fig_width,fig_height]
  #fig_size=[2.5736820257368205,1.5906229681400368]#half size
  #fig_size=[3.4315760343157606,2.120830624186716] #2/3

  params = {#'backend': 'pdf',
            'axes.labelsize': fontsize,
            'font.size': fontsize,
            'axes.titlesize': fontsize,
            'legend.fontsize': fontsize,
            'xtick.labelsize': fontsize,
            'ytick.labelsize': fontsize,
            'figure.figsize': fig_size,
            'axes.grid': False,
            'legend.frameon':True,
            'legend.borderpad': 0.5,
            'legend.labelspacing': 0.1,
            'legend.handletextpad': 0.3,
            'legend.handlelength':0.4,
            'legend.fancybox':False,
            'mathtext.default':'sf',
            'lines.linewidth':1.,
            'patch.linewidth':1.,
            'axes.linewidth':1.,
            'xtick.major.width':1.,
            'ytick.major.width':1.,
            'xtick.major.pad':5,
            'xtick.minor.pad':5,
            'ytick.major.pad':5,
            'ytick.minor.pad':5,
            'mathtext.fontset':'cm',
            'pdf.fonttype' : 42,
            }
  plt.rcParams.update(params)

def colors(numcolors,map='Spectral'):
    std_col = ['r','b','g','m','orange','k']
    if numcolors <= len(std_col):
        return std_col[:numcolors]
    cm=plt.get_cmap(map)
    col = []
    for i in range(numcolors):
        col.append(cm(1.*i/numcolors))
    return col

def shapes(nshapes):
    std_shapes = ['o','v','s','*','>','x','d',\
            'p','h','H','D','+','|','_']
    return std_shapes[:nshapes]

def legendTextColor(leg,colors):
    for color,text in zip(colors,leg.get_texts()):
        text.set_color(color)

IPPblue = '#326cb3'
IPPred = '#d1484d'
IPPgreen = '#008b29'
