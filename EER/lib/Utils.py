"""
Functions for basic signal analysis
"""
__author__ = 'mcavedon@ipp.mpg.de'
__date__ = '20.03.15'
__version__ = '0.1'

import numpy as np
import matplotlib.pylab as plt
import logging

def ConditionalSynchronization(time,data,timeBaseSync,output=None):
    """
    Conditional Syncronitzation routine based respect to the input
    timeBaseSync
    """
    if data.shape[0] != time.size:
        raise Exception('Input data axis 0 does not have same'+\
                        ' dimension of time')
    # Find proper time windows in terms in index 
    jEvBeg = np.min(np.where(timeBaseSync > time[0]))
    jEvEnd = np.max(np.where(timeBaseSync < time[-1]))
    inputDataShape = np.array(data.shape)
    outputDataShape = np.append(0,\
                                inputDataShape[1:])
    NewTime = np.array([])
    TimeNotSync = np.array([])
    NewData = np.zeros(outputDataShape)
    j0 = 0
    j0Iposola = 0
    for jEv in xrange(jEvBeg,jEvEnd+1):
        logging.info('%d\t%.3f'%(jEv,timeBaseSync[jEv]))
        # Select window in between event-1,event,event+1
        jtNull = np.argmin(np.abs(time-timeBaseSync[jEv]))
        if jEv == 0:
            jtPre = 0
        else:
            jtPre = np.argmin(np.abs(time-timeBaseSync[jEv-1]))
        if jEv == len(timeBaseSync)-1:
            jtNext = -1
        else:
            jtNext = np.argmin(np.abs(time-timeBaseSync[jEv+1]))
        logging.info('Event: %.5f\tBefore: %.5f\tAfter: %.5f'\
                     %(timeBaseSync[jEv],time[jtPre],time[jtNext]))
        NewTime = np.append(NewTime,time[jtPre:jtNext]-timeBaseSync[jEv])
        TimeNotSync = np.append(TimeNotSync,time[jtPre:jtNext])
        NewData = np.append(NewData,data[jtPre:jtNext],axis=0)
    # Re-ordering
    index = np.argsort(NewTime)
    TimeNotSync = TimeNotSync[index]
    NewTime = NewTime[index]
    NewData = NewData[index]
    if output == 'full' or output == True:
        return NewTime,NewData,timeBaseSync[jEvBeg:jEvEnd+1],index,TimeNotSync
    return NewTime,NewData

def Bin(temp,n,std=False):
    """
    Simple binning function
    """
    out = temp.copy()
    size = temp.shape[0]
    if size%n > 0:
        out = out[:-(size%n)]
    newshape = (int(temp.shape[0]/n),n)
    newshape = np.append(newshape,temp.shape[1:])
    newshape = np.array(newshape,dtype=np.int)
    out = out.reshape(newshape)
    if std:
        return out.mean(axis=1),out.std(axis=1)
    else:
        return out.mean(axis=1)

def CentralMovingAverage(temp,n=1,time=False,ifstd=False):
    """
    Moving average or also called running mean
    """
    m = n
    if m == 0:
        return temp
    avg = temp.copy()
    iftime = False
    try:
        if len(time):
            iftime = True
    except:
        pass
    if ifstd:
        std = np.zeros_like(avg)
    if iftime:
        av_diff = np.average(np.diff(time))*1.2
    for i in range(n, temp.shape[0]-m):
        if iftime:
            if np.average(np.diff(time[i-n:i+m+1])) > av_diff:
                avg[i] = np.NaN
                continue
        window = temp[i-n:i+m+1]
        avg[i] = np.average(window,axis=0)
        if ifstd:
            std[i] = np.std(window,axis=0)
    avg[:n] = np.NaN
    avg[-m:] = np.NaN
    if ifstd:
        std[:n] = np.NaN
        std[-m:] = np.NaN
        return avg,std
    return avg

if __name__=='__main__':
    import matplotlib.pylab as plt
    from dd.dd import shotfile
    shot = 30729
    ExpELM = 'FLAG'
    EdELM = 5
    elm = shotfile('ELM',shot,ExpELM,EdELM)
    timeBaseSync = elm('t_begELM')
    mac = shotfile('MAC',shot)
    ipol = mac('Ipolsola')(2.95,3.0)
    NewTime,NewData = ConditionalSynchronization(ipol.time,\
                                                 ipol.data,\
                                                 timeBaseSync)
    plt.plot(NewTime*1.e3,NewData,'.')
    plt.show()

