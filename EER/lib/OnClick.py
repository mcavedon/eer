from pylab import arange,pi,sin,cos,sqrt
import matplotlib.pylab as plt
from matplotlib.font_manager import FontProperties
import numpy as np
from matplotlib.ticker import ScalarFormatter,MaxNLocator
from math import log10, floor
from pylab import get_cmap
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.transforms import TransformedBbox, Affine2D

def on_click(event):
    """
    Enlarge or restore the selected axis.
    Usage: fig.canvas.mpl_connect('button_press_event', fconf.on_click)
    """
    ax = event.inaxes
    full_screen_pos = (0.1, 0.1, 0.85, 0.85)
    #if (np.array(full_screen_pos) - np.array(ax.get_position().bounds)) <= 0.01:
        #print 'Works'
    if ax is None:
        # Occurs when a region not in an axis is clicked...
        return
    if event.button is 3:
        # On right click, zoom the selected axes
        if not (np.abs(np.array(full_screen_pos) - np.array(ax.get_position().bounds))).sum() <= 0.01:
            ax._orig_position = ax.get_position()
            ax.set_position([0.1, 0.1, 0.85, 0.85])
            for axis in event.canvas.figure.axes:
            # Hide all the other axes...
                if axis is not ax:
                    axis.set_visible(False)
    #elif event.button is 3:
        # On right click, restore the axes
        else:
            try:
                ax.set_position(ax._orig_position)
                for axis in event.canvas.figure.axes:
                    axis.set_visible(True)
            except AttributeError:
            ## If we haven't zoomed, ignore...
                pass
    else:
        # No need to re-draw the canvas if it's not a left or right click
        return
    event.canvas.draw()

def Click2Zoom(fig):
    fig.canvas.mpl_connect('button_press_event', on_click)
