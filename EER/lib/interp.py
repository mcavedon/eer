from scipy.interpolate import interp1d
from scipy import arange, array, exp
import numpy as np

def extrap(x, xp, yp):
    """np.interp function with linear extrapolation"""
    y = np.interp(x, xp, yp)
    #y = np.where(x<xp[0], yp[0]+(x-xp[0])*(yp[0]-yp[1])/(xp[0]-xp[1]), y)
    #y = np.where(x>xp[-1], yp[-1]+(x-xp[-1])*(yp[-1]-yp[-2])/(xp[-1]-xp[-2]), y)
    return y


class poly1d:
    def __init__(self,coeff,max,min,fill_value=np.nan):
        self.poly = np.poly1d(coeff)
        self.max = max
        self.min = min
        self.fill_value = fill_value

    def __call__(self,x):
        y = self.poly(x)
        y[x<self.min] = self.fill_value
        y[x>self.max] = self.fill_value
        return y
