# EER

Routine to calculate the Edge Radial Electric Field from Edge CXRS measuraments.

## Brief Description

Calculation of the Edge Er base on the Edge CXRS measuraments in a
semi-automatic way. The results are saved in a shotfile EER.

Please look in examples/ for instruction how to run the routine.

## ToDo

 - Error bars on Er (Gaussian Process?)
 - Consistency checks of the time-bases
 - Implementation of the impurity density calculation


