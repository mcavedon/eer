import sys
sys.path.append('../')
import EER
from EER.lib import CXZ, IDA
import numpy as np
import logging


logging.basicConfig(level=logging.DEBUG)

shot = 37145
tBeg = 3
tEnd = 4.00
shiftCPZ = -0.005
shiftCMZ = 0.00


# Read CPZ
cpz = CXZ.CXZ('CPZ',shot,experiment='MCAVEDON')
cpz.readProfiles()
cpz = cpz.selectTime(tBeg,tEnd)
cpz.fluxCoordinate()
cpz.shiftProfiles(shiftCPZ)
cpz.normalizeIntensity(plot=False,normalize=True,\
        factor=np.array([1.3,1.2]),los_name=['CPR-2','CPR-5'])

## Read CMZ
cmz = CXZ.CXZ('CMZ',shot,experiment='MCAVEDON')
cmz.readProfiles()
cmz = cmz.selectTime(tBeg,tEnd)
cmz.fluxCoordinate()
cmz.shiftProfiles(shiftCMZ)
cmz.normalizeIntensity(plot=False,normalize=True,\
        factor=np.array([1.3,1.1,1.2,1.1]),\
        los_name=['CMR-1-3','CMR-1-2','CMR-1-5','CMR-1-7'])

ida = None

erfast = EER.EER(cmz,cpz,ida)
#erfast.check_alignment()
erfast.fit_profiles(plot=False,plot_IDA=False,sTi=0.11,sInte=600.0)
erfast.calculate_er(plot=False)
erfast.write_sf(experiment='MCAVEDON',path2sh='../EER/')
